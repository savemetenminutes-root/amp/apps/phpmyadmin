#!/bin/bash

. wait_for_mariadb.sh
. wait_for_mysql.sh

cd /var/www/content/phpmyadmin

ssh-keyscan -H github.com >> /root/.ssh/known_hosts
php -d memory_limi=-1 -d max_execution_time=0 ../composer.phar install --no-interaction
